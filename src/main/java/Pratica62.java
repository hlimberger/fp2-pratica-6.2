/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
import utfpr.ct.dainf.if62c.pratica.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collections;


public class Pratica62 {
    public static void main(String[] args) {
       
        Time cam = new Time();
        
        cam.addJogador("Goleiro", new Jogador(4, "Victor"));
        cam.addJogador("Zagueiro", new Jogador(40, "Cleiton"));
        cam.addJogador("Meia", new Jogador(16, "Giovanni"));
        cam.addJogador("Volante", new Jogador(3, "Leonardo Silva"));
        cam.addJogador("Atacante", new Jogador(1, "Fricson Erazo"));
        cam.addJogador("Lateral", new Jogador(16, "Rodrigão"));
        cam.addJogador("Centro-Avante", new Jogador(30, "Cleiton"));
        cam.addJogador("Ala", new Jogador(16, "Cleiton"));
        
        List<Jogador> lista = cam.ordena(new JogadorComparator(false,true,false));
        
        Iterator<Jogador> it = lista.iterator();
        
        System.out.println("Lista de jogadores:");
        while(it.hasNext()){
            Jogador j = it.next();
            System.out.println(j.toString());
        }
        
        Jogador jogador = new Jogador(16, "Rodrigão");
        
        int index = Collections.binarySearch(lista, jogador);
        
        System.out.println("Jogador encontrado: " + index);
        
        
        // Testes
        Jogador jog1 = new Jogador(1, "a");
        Jogador jog2 = new Jogador(1, "a");
        
        JogadorComparator jg = new JogadorComparator(false, false, false);
        int resultado = jg.compare(jog1, jog2);
        System.out.println("Comparando: " + resultado);
        System.out.println(jog1.getNumero() + " - " + jog1.getNome());
        System.out.println(jog2.getNumero() + " - " + jog2.getNome());
    }
}
